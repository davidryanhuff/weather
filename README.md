# Weather App

A simple weather app using Angular and D3js to visualize correlations between data

check console log for behind the scenes detailed information

## Build & development

Run `grunt` for building and `grunt serve` for preview.

Tasks
1. Get current location
2. Request data from forecast.io
3. Collect Data: pressure & windspeed
4. Graph data on scatterplot
5. Tooltip for specific info