'use strict';

/**
 * @ngdoc overview
 * @name weatherApp
 * @description
 * # weatherApp
 *
 * Main module of the application.
 */
angular
  .module('weatherApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .run(function($rootScope, $location) {
    $location.path('/');
    $rootScope.graphData = [];
  })
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/graphData', {
        templateUrl: 'views/graphdata.html',
        controller: 'GraphdataCtrl',
        controllerAs: 'graphData'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
