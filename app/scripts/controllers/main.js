'use strict';

/**
 * @ngdoc function
 * @name weatherApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the weatherApp
 */
angular.module('weatherApp')
  .controller('MainCtrl', ['$rootScope', '$scope', 'geoLocation', 'weather', '$location', '$timeout', function($rootScope, $scope, geoLocation, weather, $location, $timeout) {
    $scope.dataLoaded = true;
    $scope.checkLocation = function() {
      // confirm function call
      console.log('button clicked');
      $scope.dataLoaded = false;

      // call to geolocation Service to get current location
      geoLocation.get()
        .then(function(position) {
          $scope.dataLoaded = true;
          console.log('main ctrl, currentposition', position);
          $rootScope.position = position.latitude.toFixed(4) + ', ' + position.longitude.toFixed(4);
          $scope.getWeather(position.latitude, position.longitude);
        });
    };

    $scope.getWeather = function(lat, lng) {
      weather.get(lat, lng)
        .then(function(data) {
          console.log('main ctrl, weather data', data);
          $scope.collectGraphData(data);
        });
    };

    $scope.collectGraphData = function(data) {

      var i;
      for (i = 0; i < data.hourly.data.length; i++) {

        $rootScope.graphData.push({
          'pressure': data.hourly.data[i].pressure,
          'windSpeed': data.hourly.data[i].windSpeed
        });
      }
      console.log($rootScope.graphData);
      $timeout(function() {
        $location.path('/graphData');
      }, 1000);

    };


  }]); // end MainCtrl
