'use strict';

/**
 * @ngdoc service
 * @name weatherApp.weather
 * @description
 * # weather
 * Factory in the weatherApp.
 */
angular.module('weatherApp')
  .factory('weather', ['$q', '$http', function ($q, $http) {

    return {
      get: function (lat,lng) {
        console.log('get weather factory',lat,lng);

        var deferred = $q.defer(),
            url = 'https://api.forecast.io/forecast/a46718974277e5ef223397086b3d6b97/' + lat + ',' + lng +'?callback=JSON_CALLBACK';
            console.log('url = ', url);

        $http.jsonp(url)
        .success(function(data){
          deferred.resolve(data);
        })
        .error(function(){
          console.log('ajax error');
          deferred.reject();
        });

        return deferred.promise;

      }
    };
  }]);
