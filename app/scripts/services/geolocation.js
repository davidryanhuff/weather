'use strict';

/**
 * @ngdoc service
 * @name weatherApp.geoLocation
 * @description
 * # geoLocation
 * Factory in the weatherApp.
 */
angular.module('weatherApp')
  .factory('geoLocation', ['$q', '$rootScope', function($q, $rootScope) {

    // Public API here
    return {
      get: function() {

        var deferred = $q.defer();

        function success(position) {
          $rootScope.$apply(function() {
            deferred.resolve(position.coords);
          });
        }

        function error() {
          $rootScope.$apply(function() {
            console.log('could not get current location');
            deferred.reject();
          });
        }

        window.navigator.geolocation.getCurrentPosition(success, error);

        return deferred.promise;
      }
    };
  }]);
