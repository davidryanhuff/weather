'use strict';

describe('Controller: GraphdataCtrl', function () {

  // load the controller's module
  beforeEach(module('weatherApp'));

  var GraphdataCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GraphdataCtrl = $controller('GraphdataCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(GraphdataCtrl.awesomeThings.length).toBe(3);
  });
});
